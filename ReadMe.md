 SPARTACUS - little script for personal use

 ...is a small script that solves my personal problem of managing
 members and their monthly payments in my recreational boxing group.
 This way I will have overview of members attendance and their payments
 throughout a season.
 
 
 Its functions are:
 
 1. documenting months in which member was active
 2. total number of months a member was active throughout a season
 3. counting all payments made by each member
 4. counting up to date payment sum of all members 
 5. providing list of all members who made at least one payment
 6. adding made payments for each month
 7. displaying relevant info of a member by request


Libraries used:
  1. PStore (for storing members as objects)
  2. CSV (for inputing information)
