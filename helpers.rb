# formatting helpers

def divider
  divide = "--  " * 8
  puts divide.center(115)
end

def breakline
  print "\n"
end

def separator(string)
	breakline
	puts string
	breakline
end

def break_divide
	breakline
	divider
end
