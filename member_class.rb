class Member
  attr_accessor :name, :surname, :active_months, :payments

  def initialize(name, surname)
    @name = name
    @surname = surname
    @active_months = Array.new
    @active_months << Time.now.strftime("%m").to_i
    @payments = 150
  end

  def new_payment
    @payments += 150
    @active_months << Time.now.strftime("%m").to_i
  end

  def active_months_sum
    @active_months.size
  end
end
