require 'pstore'
require 'csv'
require_relative 'member_class'
require_relative 'helpers'

breakline
puts "(enter a number for an action )".center(111)

loop do
  breakline
  puts " M A I N    M E N U".center(107)
  puts "-".center(110)
  puts "1] add revenues".center(109)
  puts "2] get group info".center(111)
  puts "3] get member info".center(113)
  puts "4] exit\n".center(103)
  break_divide
  box = Array.new
  db = PStore.new("dataBase") # opening PStore file and storing its content in the 'db' variable

  db.transaction do
    db[:members].each { |member| box << member } if db[:members]  # storing each member from the db to 'box' array
  end

  breakline
  print String.new.rjust(53)
  case gets.rjust(54).to_i
  when 1 # |1| add revenues and members
    breakline
    puts "1.1] choose a file".center(110)
    puts "1.2] main menu\n".center(108)
    print String.new.rjust(53)
    choice = gets.to_i
    submenu_revenue = true
    break_divide
    while submenu_revenue
      if choice == 1 # |1.1| inserting file to parse
        separator("Enter a name of a file : ".center(114))
        print String.new.rjust(51)
        file_name = gets.chomp
        breakline

        begin # rescuing errors in a case of a 'file_name' that doesn't exist
          csv_members = CSV.read("#{file_name}.txt")
          db.transaction do
            db[:members] ||= Array.new
            db[:name_list] ||= Array.new # initializing 'name_list' for full names of members
            csv_members.each_with_index do |csv_member, index|
              csv_member_name = "#{csv_member[0].downcase} #{csv_member[1].downcase}"
              if db[:name_list].include? csv_member_name
                db[:members].each do |member|
                  db_member_name = "#{member.name} #{member.surname}"
                  member.new_payment if csv_member_name == db_member_name
                end
              else
                db[:members] << Member.new(csv_member[0].downcase, csv_member[1].downcase)
                db[:name_list] << "#{csv_member[0].downcase} #{csv_member[1].downcase}" # downcaseing string value for safety
                puts "#{index+1}. #{csv_member[0].capitalize} #{csv_member[1].capitalize} added.".center(110)
                breakline
              end
            end
            submenu_revenue = false
            divider
          end
        rescue
          puts "There is no file in this directory with a given name.".center(107)
          break_divide
        end
      elsif choice == 2 # |1.2| back to main menu
        submenu_revenue = false
      else # |1.3| invalid request handle
        separator("Choose one of the given options.".center(110))
        submenu_revenue = false
        divider
      end
    end
  when 2 # |2| fetch information on the group
    breakline
    puts "Choose an operation you want to execute on a group : ".center(112)
    submenu_group = true # using submenu_group value to control the while loop

    while submenu_group
      separator("O  p  t  i  o  n  s: ".center(112))
      puts "1] number of members".center(106)
      puts "2] full names of members".center(111)
      puts "3] sum of payments in months".center(114)
      puts "4] sum of all payments (up to date)".center(121)
      puts "5] main menu".center(98)
      breakline
      print String.new.rjust(54)
      operation = gets.chomp.to_i
      break_divide
      if operation == 1 # |2.1| number of members in the DB
        separator("There are [#{box.size.to_s} members] in the database.".center(113))
        divider
      elsif operation == 2 # |2.2| list of members in the DB
        index = 1
        breakline
        if !box.empty?
          box.each do |member|
            puts "#{index.to_s}. #{member.name.capitalize} #{member.surname.capitalize}".center(110)
            index += 1
          end
          break_divide
        else
          puts "There are no members in the database.".center(113)
          break_divide
        end
      elsif operation == 3 # |2.3| sum of payments in each month
        month = 1
        monthly_profits = Hash.new
        12.times do # opted for times method instead of hardcoding months in an array
          profit = 0
          box.each do |member|
            profit += 150 if member.active_months.include?(month)
          end
          monthly_profits[month] = profit
          month += 1
        end
        separator("Profit in each month: ".center(112))
        monthly_profits.each do |month, payments|
          puts "#{month} : #{payments}".center(110)
        end
        break_divide
      elsif operation == 4 # |2.4| up to date sum
        payment_sum = 0
        box.each do |member|
          payment_sum += member.payments
        end
        separator("Payment sum : #{payment_sum.to_s}".center(110))
        divider
      elsif operation == 5 # |2.5| back to main menu
        submenu_group = false
      else # |2.6| invalid request handle
        separator("You chose an invalid operation. Choose again!".center(114))
        divider
      end
    end
  when 3 # |3| inspect a member
    separator("Choose a name of a member you want to inspect : ".center(113))
    print String.new.rjust(50)
    member_name = gets.chomp.downcase
    found = false
    breakline

    if !box.empty?
      box.each do |member|
        if member_name == "#{member.name} #{member.surname}"
          puts  "Name: #{member.name.capitalize}".center(110)
          puts "Surname: #{member.surname.capitalize}".center(110)
          puts "Active months : #{member.active_months.to_s}".center(110)
          puts "Number of active months : #{member.active_months_sum.to_s}".center(110)
          puts "Payment sum : #{member.payments.to_s}".center(110)
          found = true
        end
      end
      puts "There is no member under that name.".center(110) if found == false
      break_divide
    else
      separator("Database is empty.".center(113))
      divider
    end
  when 4 # |4| exit the program
    separator("Fare well, Spartan!".center(110))
    exit
  when
    separator("Please choose one of the available options.".center(110))
    divider
  end
end
